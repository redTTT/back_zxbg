package com.hongtao.service;

import com.hongtao.pojo.Employee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hongtao
 * @since 2021-02-21
 */
public interface IEmployeeService extends IService<Employee> {

}
