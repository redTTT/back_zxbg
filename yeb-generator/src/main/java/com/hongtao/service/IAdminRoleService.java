package com.hongtao.service;

import com.hongtao.pojo.AdminRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hongtao
 * @since 2021-02-21
 */
public interface IAdminRoleService extends IService<AdminRole> {

}
