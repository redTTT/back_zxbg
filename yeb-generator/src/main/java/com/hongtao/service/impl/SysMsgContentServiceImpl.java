package com.hongtao.service.impl;

import com.hongtao.pojo.SysMsgContent;
import com.hongtao.mapper.SysMsgContentMapper;
import com.hongtao.service.ISysMsgContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hongtao
 * @since 2021-02-21
 */
@Service
public class SysMsgContentServiceImpl extends ServiceImpl<SysMsgContentMapper, SysMsgContent> implements ISysMsgContentService {

}
