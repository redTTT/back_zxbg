package com.hongtao.service.impl;

import com.hongtao.pojo.Admin;
import com.hongtao.mapper.AdminMapper;
import com.hongtao.service.IAdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hongtao
 * @since 2021-02-21
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService {

}
