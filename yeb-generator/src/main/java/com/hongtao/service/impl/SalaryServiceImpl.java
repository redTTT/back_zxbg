package com.hongtao.service.impl;

import com.hongtao.pojo.Salary;
import com.hongtao.mapper.SalaryMapper;
import com.hongtao.service.ISalaryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hongtao
 * @since 2021-02-21
 */
@Service
public class SalaryServiceImpl extends ServiceImpl<SalaryMapper, Salary> implements ISalaryService {

}
