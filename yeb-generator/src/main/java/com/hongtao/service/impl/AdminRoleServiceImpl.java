package com.hongtao.service.impl;

import com.hongtao.pojo.AdminRole;
import com.hongtao.mapper.AdminRoleMapper;
import com.hongtao.service.IAdminRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hongtao
 * @since 2021-02-21
 */
@Service
public class AdminRoleServiceImpl extends ServiceImpl<AdminRoleMapper, AdminRole> implements IAdminRoleService {

}
