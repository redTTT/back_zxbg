package com.hongtao.service.impl;

import com.hongtao.pojo.SysMsg;
import com.hongtao.mapper.SysMsgMapper;
import com.hongtao.service.ISysMsgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hongtao
 * @since 2021-02-21
 */
@Service
public class SysMsgServiceImpl extends ServiceImpl<SysMsgMapper, SysMsg> implements ISysMsgService {

}
