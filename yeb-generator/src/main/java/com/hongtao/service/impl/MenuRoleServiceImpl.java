package com.hongtao.service.impl;

import com.hongtao.pojo.MenuRole;
import com.hongtao.mapper.MenuRoleMapper;
import com.hongtao.service.IMenuRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hongtao
 * @since 2021-02-21
 */
@Service
public class MenuRoleServiceImpl extends ServiceImpl<MenuRoleMapper, MenuRole> implements IMenuRoleService {

}
