package com.hongtao.mapper;

import com.hongtao.pojo.SysMsgContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hongtao
 * @since 2021-02-21
 */
public interface SysMsgContentMapper extends BaseMapper<SysMsgContent> {

}
