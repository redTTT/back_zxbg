package com.hongtao.mapper;

import com.hongtao.pojo.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hongtao
 * @since 2021-02-21
 */
public interface AdminMapper extends BaseMapper<Admin> {

}
